#!/bin/bash
function usage() {
	echo "usage: <File> [<dest Dir>]"
	exit 1
}
if [ -z $1 ]; then
	usage
fi
DESTDIR=
MOUNTPOINT=mountpoint
FILE=$1
if [ ! -z $2 ]; then
	DESTDIR=$2
fi
if [ ! -e "$MOUNTPOINT" ]; then
	mkdir $MOUNTPOINT
fi
GID=`id -g`
sudo /bin/mount //ntsg12/gruppen/Transfer/awr mountpoint -o rw,user=awr,uid=$UID,gid=$GID
if test $? != 0; then 
	echo "mount error"; 
	exit 1
fi

if [ ! -z $DESTDIR ]; then 
	if [ ! -e "$MOUNTPOINT/$DESTDIR" ]; then
		mkdir -p $MOUNTPOINT/$DESTDIR
	fi
fi

cp $FILE $MOUNTPOINT/$DESTDIR

sudo /bin/umount $MOUNTPOINT
if [ $? != 0 ]; then
	echo "umount error"
	exit 1
fi
