#The target to run latex on - there should be a <target>.tex and a <target>.bib file in the project directory
TARGET=thesis

#A list of source files make should check for modifications
SOURCES=*.tex *.bib chapters/*.tex
PSSOURCES=images/*.eps images/*.svg
#PDFSOURCES=$(patsubst %.pdf,%.eps, $(PSSOURCES))

PSPAGELAYOUT=a4

SUBDIR=

all: subdir pdf

########################################################
# There is no need to modify anything below this point #
########################################################
dvi: $(TARGET).dvi

dvishow: dvi
	xdvi $(TARGET).dvi&

$(TARGET).dvi: sources pssources
	latex $(TARGET) \
	latex $(TARGET) \
	&& makeindex -s $(TARGET).ist -t $(TARGET).alg -o $(TARGET).acr $(TARGET).acn \
	&& makeindex -s $(TARGET).ist -t $(TARGET).glg -o $(TARGET).gls $(TARGET).glo \
	&& bibtex $(TARGET) \
	&& bibtex $(TARGET) \
	&& latex $(TARGET) \
	&& latex $(TARGET) \
	&& latex $(TARGET) \
	&& makeindex -s $(TARGET).ist -t $(TARGET).alg -o $(TARGET).acr $(TARGET).acn \
	&& makeindex -s $(TARGET).ist -t $(TARGET).glg -o $(TARGET).gls $(TARGET).glo \
	&& latex $(TARGET) \
	&& latex $(TARGET) \
	&& latex $(TARGET) 

psshow: ps
	gv $(TARGET).ps&

ps: $(TARGET).ps

$(TARGET).ps: dvi
	dvips -X 600 -Y 600 -t $(PSPAGELAYOUT) -o $(TARGET).ps $(TARGET).dvi

im: $(TARGET).pdf

pdfshow: pdf
	acroread $(TARGET).pdf

$(TARGET).pdf: sources pdfsources
	pdflatex $(TARGET) 
	pdflatex $(TARGET) 
	bibtex $(TARGET) 
	bibtex $(TARGET) 
	makeglossaries $(TARGET) 
	makeglossaries $(TARGET) 	
	pdflatex $(TARGET) 
	pdflatex $(TARGET) 
	pdflatex $(TARGET) 
	bibtex $(TARGET) 
	bibtex $(TARGET) 
	makeglossaries $(TARGET) 
	makeglossaries $(TARGET) 
	pdflatex $(TARGET) 
	pdflatex $(TARGET)
	pdflatex $(TARGET) 

pdf: $(TARGET).pdf

clean:
	@for d in $(SUBDIR); do \
		echo "Cleaning $$d..."; \
		make -C $$d $@; \
	done
	-rm -f *.backup *~ $(TARGET).aux $(TARGET).lof $(TARGET).lot $(TARGET).toc $(TARGET).dvi $(TARGET).log $(TARGET).ps $(TARGET).pdf $(TARGET).bbl $(TARGET).blg $(TARGET).out $(TARGET).bst $(TARGET).inx images/*~ *.bak $(TARGET).svn $(TARGET).lol $(TARGET).fot $(TARGET).idx $(TARGET).cb $(TARGET).loa $(TARGET).abk.aux $(TARGET).gls.aux *.cb2 *.acn *.acr *.alg *.glg *.glo* *.gls* *.ist *.mw *.xdy *.glg2

sources: $(SOURCES)

pssources: $(PSSOURCES)

pdfsources: $(PDFSOURCES)
	./convert.sh $(PSSOURCES)
updateSubVersion: version.sh
	./version.sh -s
updateVersion: version.sh
	./version.sh -v
upload: upload.sh updateSubVersion clean $(TARGET).pdf
	./upload.sh $(TARGET).pdf
	git add version.tex
	git commit -m "v`cat version.tex`"
	git tag v`cat version.tex`
	git push origin master v`cat version.tex`
upload_sub: $(SUBDIR)
	@for d in $(SUBDIR); do \
		echo "Building $$d..."; \
		make -C $$d upload; \
	done

subdir: $(SUBDIR)
	@for d in $(SUBDIR); do \
		echo "Building $$d..."; \
		make -C $$d all; \
	done
pack: clean
	cd ..; \
	mkdir thesis_`cat thesis/version.tex`; \
	cp -r thesis thesis_`cat thesis/version.tex`; \
	zip -r thesis_`cat thesis/version.tex`.zip thesis_`cat thesis/version.tex`; \
	rm -R thesis_`cat thesis/version.tex`;
