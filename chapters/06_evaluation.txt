- Evulation
%	- Da die vollständige Implementierung dem Rahmen der Arbeit sprengen würde wird hier Theoretische Betrachtet ob das Design sich eignet verschieden höre Betriebssystemschnittstellen auf dem System zu implementieren
%	- System bietet die Möglichkeit Abwendungen direkt auf System zu schreiben aber Anwender wollen häufig bekannte Schnittstellen wie AUTOSAR, ARINC, POSIX verwenden um Entwicklungskosten und Einarbeitung Kosten zu sparen.
%	- Da auf den genauen Inhalt der Schnittstellen aus Zeitgründen nicht weiter eingegangen werden kann -> wird untersucht ob die Standard Paradigmen die alle Schnittstellen auf die eine oder andre weiße Benutzen umsetzbar sind. 
%	- Alle Schnittstellen können auf dem vCPU Interface implementiert werden, dies wird nicht weiter beatetet. 
%	- Folgende Syncronisirungs und Komuinkationsparadigmen werden untersucht:
%		- Nachrichten Austausch 
%		- Events
%		- Semaphores und Mutexe
%		- Barrieren 
%		- Monitore
%	- Nachricht Austausch
%		- Verschieden Varianten: synchron Nachrichten Austausch, asynchron Nachrichten Austausch
%		- Mit den Varianten: gepuffert Nachrichten Austausch und ungepuffert Nachrichten Austausch
%		- Austausch der Nachrichten erfolgt zwischen Threads
%
%		- synchron Nachrichten Austausch werden gut untersützt über das \gls{ipc} Interface, wobei immer gepuffert wird. 
%			- Die Nachrichten maximale Nachrichten Größe wird durch die große der Virtuellen Register beschädigt. 
%			- Im Dynamischen Fall ist die Weitergabe von Capabilitys über das \gls{ipc} Interface möglich, was den Datenautausch 
%		- asynchron Nachrichten Austausch wird nicht unterstütze kann aber über ein \gls{shm} und die Events implementiert werden, wobei besonders auf Caches geachtet werden. 
%			- Hierbei die große nicht eingeschenkt. 
%	- Events
%		- Senden und Empfangen von autretenenen Asyncronen Events 
%
%		- Gute Unterstützung: Direkt abbildbar auf Events maximal 64 Events möglich. 
%		- Mehr durch Verschachtelung beliebig erweiterbar. 
%		- Koppelbar mit andren Ereignisse wie Interrupt, Traps und Enpoints
%	- Semaphores und Mutexe
%		- Semaphores sind E. W. Dijkstra vorgestellte Model um beispielsweiße um eine Retourenveraltung oder Locks zu realisieren
%		- Betetet aus Zählvariable zwei atomaren Operation: Down und Up
%		- Die Down Operation verringert die Zählvariable bis zur Zahl 0, falls die Zahl 0 erreicht worden ist wird der Thread solange blockiert bis die Operation erflogreich durchfüren kann
%		- Die Up Operation erhöht die Zählvariable und weckt falls vorhanden einer der Schaffende Threads
%		- Bei Mutexen (binare Semaphore) kann die Zählvariable maximal nur null oder eins annehmen. 
%		- Bei Mutexe wird der maximal Wert der Zählvariable auf eins definiert. [ref Tanenbaum:2014]
%
%		- Implementierung gut abstrahieren über die Endpoints die mehre Empfänger und Sender haben. Es werden Tickets vergeben nur wenn man ein Ticket besitzt darf man arbeiten. Die Up Operation entspricht einer Senden (mit Methode send) einer Nachricht (zugabe des Ticket) und die Down Operation entspricht dem empfangen (mit Methode recv) einer Nachricht (Ticket bekommen).
%		- Prio Inverasion möglich Lösung:
%			- Prio Invasion erklären (einfachster Fall binares Semaphore):
%				- Niderprioerer Thread hat Ticket
%				- Mittel Primärer Thread wird Rechenwillig und wird dran gekommen und unterbricht Nieder Piroerer Thread
%				- Hoch Prioerer Thread will Ticket haben und bekommt keins und warten
%				- Mittler Thread gibt Prozessor nicht wieder frei -> Prio Inverasion
%			- Lösung
%				-Semaphore Thread der die Verwaltung der Thread Prio übernimmt Kommunikation erfolgt über ein RPC Protkoll 
%				- Erweiterung der Endpoint so das das der Kern weiß das es sich um eine Lock handelt
%		- Implementierung mäsig abstrahierbar: Über eine Semaphor Server Thread der die über ein RPC Protkoll übernimmt
%	- Barrieren
%		- Synchronisieren mehre Threads an einem Punkt im Code
%		
%		- Einfach Realisierung über Events.
%		- Jeder Threads bekommt ein Event Nummer zugewiesen
%		- Setzt sein Bit im Event und legt sich am Event mit einer Bit Maske in dem alle Event Nummern gesetzt sind schalfen und wartet darauf das alle Bits gesetzt werden.
%		- Kern Weckt alle Threads wenn warte Bedingung zutrifft.  
%	- Monitore und Bedingungsvariablen
%		- Höherere Synchronisationsprimitive
%		- Vorgeschlagen von Hoare und Hansen
%		- Monitore schützen eine Gruppe von Prozeduren und Datenstrukturen
%		- betreten von Prozeduren und verändern der Datenstrukturen von maximal ein Thread
%		- Bedingungsvariablen (Condition Variablen) werden verwendet um bei bestimmten Bedienungen sich innerhalb der Prozeduren schlafen zu legen und wieder geweckt zu werden. 
%		- Monitor Server: 	
%			- Wird mäßig unterstützt: Realisierbar durch einen separaten Monitor Thread der den Zugang Kondoliert
%			- Monitor Thread besitzt ein RPC Interface das die Funktionen enter, leave, wait, notify, notifyAll und leave implementiert
%			- Thread ruft enter Methode beim betreten des Kritischen Abschnitte und bekommt wenn er eintreten kann vom Monitor Thread ein reply. 
%			- Innerhalb des Kritischen Abschnitte kann der Thread leave, wait, notify und notifyAll aufrufen. 
%			- Wait wartet an einer vorher definieren Bedingungsvariablen.
%			- notify benachrichtigt weckt ein Threads der an einen Bedingungsvariablen wartet. 
%			- notifyAll weckt alle an einer Bedingungsvariablen Threads
%			- leave verlässt Kritischen abschnitt
%
