\babel@toc {ngerman}{}
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Grundlagen}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Grundbegriffe}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Heterogene Multiprozessoren}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Einsatzgebiet}{7}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Betriebssystemschnittstellen}{8}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Prozesse und Threads}{10}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Scheduling}{11}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Rechte und Schutzsystem}{12}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Synchronisation}{13}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Kommunikation}{14}{subsection.2.3.5}
\contentsline {section}{\numberline {2.4}IEC 61508}{14}{section.2.4}
\contentsline {chapter}{\numberline {3}Analyse}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Hardware}{18}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Heterogene Multiprozessorsarchitekturen}{18}{subsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.1}Ti DRA7x / Ti TDAx}{18}{subsubsection.3.1.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.2}NXP S32v230}{20}{subsubsection.3.1.1.2}
\contentsline {subsection}{\numberline {3.1.2}Architektur}{21}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Kommunikation / Synchronisationen}{22}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Verallgemeinerte Struktur}{22}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}Anwendungsanforderungen}{24}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Beispiele}{24}{subsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.1.1}Steuerungsaufgaben}{24}{subsubsection.3.2.1.1}
\contentsline {subsubsection}{\numberline {3.2.1.2}Interaktiv}{25}{subsubsection.3.2.1.2}
\contentsline {subsection}{\numberline {3.2.2}Anforderungen an ein Betriebssystem}{26}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Zusammenfassung}{27}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}\IeC {\"A}u\IeC {\ss }ere Sicht: Betriebssystemschnittstelle}{27}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Prozesse / Threads}{29}{subsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.1.1}Prozesse}{30}{subsubsection.3.3.1.1}
\contentsline {subsubsection}{\numberline {3.3.1.2}Threads}{30}{subsubsection.3.3.1.2}
\contentsline {subsubsection}{\numberline {3.3.1.3}Ressourcen}{30}{subsubsection.3.3.1.3}
\contentsline {subsubsection}{\numberline {3.3.1.4}Security und Isolation}{30}{subsubsection.3.3.1.4}
\contentsline {subsection}{\numberline {3.3.2}Kommunikations- / Synchronisationsmechanismen}{31}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Dynamik}{32}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Eignung f\IeC {\"u}r HMP-Systeme}{32}{subsection.3.3.4}
\contentsline {section}{\numberline {3.4}Innere Sicht: Das Betriebssystem}{33}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Architektur}{33}{subsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.1.1}Bibliothek basiertes System}{34}{subsubsection.3.4.1.1}
\contentsline {subsubsection}{\numberline {3.4.1.2}Monolithischer Kern}{35}{subsubsection.3.4.1.2}
\contentsline {subsubsection}{\numberline {3.4.1.3}Microkern}{36}{subsubsection.3.4.1.3}
\contentsline {subsubsection}{\numberline {3.4.1.4}Hypervisor}{37}{subsubsection.3.4.1.4}
\contentsline {subsection}{\numberline {3.4.2}Kernobjektverwaltung}{38}{subsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.2.1}Multiprozessorproblematik}{39}{subsubsection.3.4.2.1}
\contentsline {subsubsection}{\numberline {3.4.2.2}Adressraum Problematik}{40}{subsubsection.3.4.2.2}
\contentsline {subsubsection}{\numberline {3.4.2.3}Cache Problematik}{41}{subsubsection.3.4.2.3}
\contentsline {chapter}{\numberline {4}Design}{43}{chapter.4}
\contentsline {section}{\numberline {4.1}Auswahl eines geeigneten Designs}{43}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Architektur}{43}{subsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.1}Hypervisor ohne Treiber}{44}{subsubsection.4.1.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.2}Mikrokern}{45}{subsubsection.4.1.1.2}
\contentsline {subsubsection}{\numberline {4.1.1.3}Hybrid: Mikrokern + Hypervisor}{45}{subsubsection.4.1.1.3}
\contentsline {subsubsection}{\numberline {4.1.1.4}Resultat}{45}{subsubsection.4.1.1.4}
\contentsline {subsection}{\numberline {4.1.2}Statisch versus dynamisch}{46}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Betriebssystemschnittstelle}{46}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Grobdesign}{46}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Kernobjekte}{47}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Interprozessor-Kommunikation}{47}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Statische Threads}{47}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Dynamische Threads}{48}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Speicherverwaltung}{48}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Scheduling}{49}{subsection.4.2.6}
\contentsline {subsection}{\numberline {4.2.7}vCPU Interface}{49}{subsection.4.2.7}
\contentsline {subsection}{\numberline {4.2.8}Betriebssystemschnittstelle}{49}{subsection.4.2.8}
\contentsline {subsection}{\numberline {4.2.9}Kommunikation und Synchronisation}{49}{subsection.4.2.9}
\contentsline {section}{\numberline {4.3}Feindesign}{50}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Speicherverwaltung}{50}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Kernobjekte und Capabilities}{50}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}Capability Listen}{50}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}CPU}{51}{subsubsection.4.3.2.2}
\contentsline {subsubsection}{\numberline {4.3.2.3}Thread Control Block (TCB)}{51}{subsubsection.4.3.2.3}
\contentsline {subsubsection}{\numberline {4.3.2.4}Endpunkt}{52}{subsubsection.4.3.2.4}
\contentsline {subsubsection}{\numberline {4.3.2.5}Interrupt}{53}{subsubsection.4.3.2.5}
\contentsline {subsubsection}{\numberline {4.3.2.6}Event}{53}{subsubsection.4.3.2.6}
\contentsline {subsubsection}{\numberline {4.3.2.7}Page Directory}{54}{subsubsection.4.3.2.7}
\contentsline {subsubsection}{\numberline {4.3.2.8}Page}{54}{subsubsection.4.3.2.8}
\contentsline {subsubsection}{\numberline {4.3.2.9}vCPU}{54}{subsubsection.4.3.2.9}
\contentsline {subsection}{\numberline {4.3.3}Interprozessor-Kommunikation}{55}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Systemcall Interface}{56}{subsection.4.3.4}
\contentsline {subsubsection}{\numberline {4.3.4.1}Klassisches Systemcall Interface}{56}{subsubsection.4.3.4.1}
\contentsline {subsubsection}{\numberline {4.3.4.2}RPC-basiertes Interface}{57}{subsubsection.4.3.4.2}
\contentsline {subsubsection}{\numberline {4.3.4.3}Finales Design}{58}{subsubsection.4.3.4.3}
\contentsline {chapter}{\numberline {5}Implementierung}{59}{chapter.5}
\contentsline {section}{\numberline {5.1}Umsetzungsplan}{59}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Betriebssystem ohne MMU mit statischen Threads auf einem Einkernsystem}{59}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Erweiterung Mehrkernfunktionalit\IeC {\"a}t}{60}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}MMU und MPU}{60}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Dynamik}{60}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Hypervisor}{61}{subsection.5.1.5}
\contentsline {chapter}{\numberline {6}Evaluation}{63}{chapter.6}
\contentsline {section}{\numberline {6.1}Nachrichtenaustausch}{64}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Synchrone Nachrichten}{64}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Asynchrone Nachrichten}{64}{subsection.6.1.2}
\contentsline {section}{\numberline {6.2}Events}{64}{section.6.2}
\contentsline {section}{\numberline {6.3}Semaphore und Mutexe}{65}{section.6.3}
\contentsline {section}{\numberline {6.4}Barrieren}{66}{section.6.4}
\contentsline {section}{\numberline {6.5}Monitore und Bedingungsvariablen}{66}{section.6.5}
\contentsline {chapter}{\numberline {7}Fazit und Ausblick}{69}{chapter.7}
\contentsline {section}{\numberline {7.1}Ausblick}{70}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Speicherverwaltung}{71}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}RPC Protokoll}{71}{subsection.7.1.2}
\contentsline {subsection}{\numberline {7.1.3}Interprozessor-Kommunikation}{71}{subsection.7.1.3}
\contentsline {subsection}{\numberline {7.1.4}H\IeC {\"o}here Betriebssystemschnittstellen}{72}{subsection.7.1.4}
\contentsline {subsection}{\numberline {7.1.5}Scheduler}{72}{subsection.7.1.5}
\contentsline {subsection}{\numberline {7.1.6}Echtzeit}{72}{subsection.7.1.6}
\contentsline {subsection}{\numberline {7.1.7}Verifikation}{72}{subsection.7.1.7}
\contentsline {chapter}{\numberline {A}Buildsystem}{73}{appendix.A}
\contentsline {section}{\numberline {A.1}Adressraum Problematik}{73}{section.A.1}
\contentsline {section}{\numberline {A.2}Compiler Problematik}{74}{section.A.2}
\contentsline {section}{\numberline {A.3}Buildsystem}{74}{section.A.3}
\contentsline {chapter}{Abk\IeC {\"u}rzungsverzeichnis}{81}{section*.8}
\contentsline {chapter}{Literaturverzeichnis}{85}{appendix*.10}
