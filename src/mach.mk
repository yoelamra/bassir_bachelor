CORES := a5-1 m4-1
BOOT_CORE := a5-1
ARM_PREFIX := arm-none-eabi-
# Default Compiler
CC := $(ARM_PREFIX)gcc
LD := $(ARM_PREFIX)ld
AR := $(ARM_PREFIX)ar
OBJCOPY := $(ARM_PREFIX)objcopy
# Load Default Settings for ARM Cortex A5
include $(srctree)/arch/arm/cortex-a5.mk
# Load Default Settings for ARM Cortex M4
include $(srctree)/arch/arm/cortex-m4.mk
# Define Compiler, Linker, Assambler, ... for A5 and M4 Core
CC_a5-1 := $(ARM_PREFIX)gcc
CC_m4-1 := $(ARM_PREFIX)gcc
LD_a5-1 := $(ARM_PREFIX)ld
LD_m4-1 := $(ARM_PREFIX)ld
AR_a5-1 := $(ARM_PREFIX)ar
AR_m4-1 := $(ARM_PREFIX)ar
OBJCOPY_a5-1 := $(ARM_PREFIX)objcopy
OBJCOPY_m4-1 := $(ARM_PREFIX)objcopy

# define Compiler, Linker, Assambler Flags for A5 and M4 Core
AFLAGS += -I$(srctree)/mach/vf610/include/
CFLAGS += -I$(srctree)/mach/vf610/include/
LDFLAGS +=
AFLAGS_a5-1 := $(AFLAGS_cortex-a5)
AFLAGS_m4-1 := $(AFLAGS_cortex-m4)
CFLAGS_a5-1 := $(CFLAGS_cortex-a5)
CFLAGS_m4-1 := $(CFLAGS_cortex-m4)
LDFLAGS_a5-1 := $(LDFLAGS_cortex-a5)
LDFLAGS_m4-1 := $(LDFLAGS_cortex-m4)

LDS_a5-1 := $(buildtree)/a5-1/$(LDS_arm)
LDS_m4-1 := $(buildtree)/m4-1/$(LDS_arm_m4)
