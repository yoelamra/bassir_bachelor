#!/bin/bash

for epsname in "$@"; do
	pathname=`dirname $epsname`
	case "$epsname" in
		*eps*)
			type=eps
		;;
		*svg*)
			type=svg
		;;
	esac
	if [ -z $pathname ]; then
		name=`basename $epsname .$type`
	else
		name=$pathname/`basename $epsname .$type`
	fi
	pdfname=$name.pdf
	if [ $epsname = $name ]; then
		echo File extension .eps or .svg expected for conversion, skipping $epsname
	else
		if [ -e "$epsname" ]; then
			if [ "$pdfname" -ot "$epsname" ]; then
				echo +++ Converting     $epsname to $pdfname
				case "$epsname" in
					*eps*)
						epstopdf $epsname
					;;
					*svg*)
						inkscape $epsname --export-pdf=$pdfname
					;;
				esac
			else
				echo --- Not converting $epsname to $pdfname
			fi
		else
			echo Source file $epsname does not exist!
		fi
	fi;
done
