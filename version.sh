#!/bin/bash
VERSION=`awk -F. '{print $1}' version.tex`
SUBVERSION=`awk -F. '{print $2}' version.tex`
function usage() {
	echo "usage: -v -s"
	exit 1
}
function updateSubVersion() {
	SUBVERSION=$(($SUBVERSION+1))
}
function updateVersion() {
	VERSION=$(($VERSION+1))
}
if [ $# == 0 ]; then
	usage
fi
for i in $@
do
	case $i in
	-v)
		updateVersion
		;;
	-s)
		updateSubVersion
		;;
	*)
		usage
		;;
	esac
done

echo "$VERSION.$SUBVERSION" > version.tex
